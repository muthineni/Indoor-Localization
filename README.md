# Indoor-Localization
Use of Arduino and XBee modules for indoor localization

XBee modules are configure in three different modes namely End Device, Router, and Coordinator using X-CTU Software.

XBee Routers are placed in three different coordinates of the room

XBee End Device is connected to the person whose movement need to be tracked.

XBee Coordinator is connected to the Desktop running MATLAB for visualizing user movement.

For more information of the project, visit: https://indiarxiv.org/afkej/
